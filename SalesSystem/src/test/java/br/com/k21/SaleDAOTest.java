package br.com.k21;

import org.junit.Test;

import br.com.k21.dao.SaleDAO;
import br.com.k21.infra.BaseDBTest;

public class SaleDAOTest extends BaseDBTest {

	@Test
	public void testInexistentSalesman() {
		// Arrange
		Salesman salesman = new Salesman();
		salesman.setId(999);
		int ano = 2017;		
		double valorTotalVendasEsperado = 0;
		double valorTotalRetornado;
		
		// act
		SaleDAO.setEntityManager(emf.createEntityManager());
		valorTotalRetornado = SaleDAO.getSalesTotalBySalesmanAndYear(salesman, ano);
		
		// asserts
		assertEquals(valorTotalVendasEsperado, valorTotalRetornado);		
	}
		
	@Test
	public void testSalesman1Retunrs142() {
		// Arrange
		Salesman salesman = new Salesman();
		salesman.setId(1);
		int ano = 2017;		
		double valorTotalVendasEsperado = 142;
		double valorTotalRetornado;
		
		// act
		SaleDAO.setEntityManager(emf.createEntityManager());
		valorTotalRetornado = SaleDAO.getSalesTotalBySalesmanAndYear(salesman, ano);
		
		// asserts
		assertEquals(valorTotalVendasEsperado, valorTotalRetornado);


	}

	@Test
	public void TestWhitNoSales ()
	{
		//Arrange
		Salesman salesman = new Salesman();
		salesman.setId(1);
		int ano = 2017;
		double totalAmountOFSales= 0;
		double returnedTotalValue = 0;

		//Act
		SaleDAO.setEntityManager(emf.createEntityManager());
		totalAmountOFSales= SaleDAO.getSalesTotalBySalesmanAndYear(salesman, ano);

		// asserts
		assertEquals(totalAmountOFSales, returnedTotalValue);

	}

	@Test
	public void Test2018salesman1 ()
	{
		//Arrange
		Salesman salesman = new Salesman();
		salesman.setId(1);
		int ano = 2018;
		double totalAmountOFSales= 0;
		double returnedTotalValue = 20000;

		//Act
		SaleDAO.setEntityManager(emf.createEntityManager());
		totalAmountOFSales= SaleDAO.getSalesTotalBySalesmanAndYear(salesman, ano);

		// asserts
		assertEquals(totalAmountOFSales, returnedTotalValue);

	}
}
