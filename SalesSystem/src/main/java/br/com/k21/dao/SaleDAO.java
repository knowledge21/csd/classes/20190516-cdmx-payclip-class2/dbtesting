package br.com.k21.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.com.k21.Sale;
import br.com.k21.Salesman;

public class SaleDAO {
	
    @PersistenceContext(unitName = "JPA")
    private static EntityManager entityManager;

    
    public static Sale find(long id)
    {
        return entityManager.find(Sale.class, new Long(id));
    }
    

    public static void remove(long id){
    	entityManager.remove(find(id));
    }
    
    public static EntityManager getEntityManager()
    {
        return entityManager;
    }

    public static void setEntityManager(EntityManager entityManager)
    {
    	SaleDAO.entityManager = entityManager;
    }


	public static double getSalesTotalBySalesmanAndYear(
			Salesman salesman,
			int ano) {
		Query q = entityManager.createNativeQuery(
				"select sum(s.value) " +
				"from sale s " +
				"where s.salesmanId = :paramSalesmanId "+
				"and s.saleDate like :saleDate ");

		q.setParameter("paramSalesmanId", salesman.getId());
		q.setParameter("saleDate", ano + "%");
		
    	Object o = q.getSingleResult();
    	if(o == null)
    		return 0;
    	
    	Double valor = (Double) o;
    	return valor;
 	}


}
